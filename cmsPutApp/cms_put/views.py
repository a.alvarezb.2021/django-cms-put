from django.shortcuts import render
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt

from .models import Contenido


# Create your views here.

def index(request):
    """
    SI me hace un GET /cms/ le doy una respueta de que esta es un cms app
    """
    return HttpResponse("Hello, this is a cms app")


@csrf_exempt
def do_MEHTOD(request, llave):
    """
    En caso de que el metodo sea GET , obtengo el el body del objeto guardado con la llave = llave
    y devuelvo su contenido. En caso de no exista contenido para esa llave salta la excepcion
    En caso de que el metodo sea PUT, obtengo el cuepor de la peticion y me creop un objeto del tipo contenido,
    guardango su llave y su valor. Luego devuelvo el body del objeto creado.
    """
    if request.method == "GET":
        try:
            contenido = Contenido.objects.get(llave=llave).valor
            return HttpResponse(f"El contenido es {contenido}")
        except Contenido.DoesNotExits:
            return HttpResponse("No exite esa ella o no existe continido para esa llave")

    elif request.method == "PUT":
        cuerpo = request.body.decode("utf-8")
        if cuerpo:
            contenido = Contenido(llave=llave, valor=cuerpo)
            contenido.save()
        try:
            respuesta = Contenido.objects.get(llave=llave).valor
            return HttpResponse(f"Guardando en un nuevo contenido la llave {llave} con el cuerpo : {respuesta}")
        except Contenido.DoesNotExist:
            return HttpResponse(f"No hay contenido para la llave: {llave}")


from django.shortcuts import render

# Create your views here.
