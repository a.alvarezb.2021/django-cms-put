from django.db import models


# Create your models here.


class Contenido(models.Model):
    """
    Base de datos para el contenido
    Sus campos son:
    llave: clave de la variable
    valor: valor de la variable
    """

    llave = models.CharField(max_length=264)
    valor = models.TextField()


class Comentario(models.Model):
    """
    Base de datos para los comentarios
    Sus campos son:
    contenido: forma de acceder a la base de datos del contenido
    comentario: comentario del usuario
    """

    contenido = models.ForeignKey(Contenido, on_delete=models.CASCADE)
    titulo = models.CharField(max_length=264)
    cuerpo = models.TextField()
    fecha = models.DateTimeField("publicado")


from django.db import models

# Create your models here.
