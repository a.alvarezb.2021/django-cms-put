# Generated by Django 5.0.3 on 2024-03-21 17:34

import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Contenido',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('llave', models.CharField(max_length=264)),
                ('valor', models.TextField()),
            ],
        ),
        migrations.CreateModel(
            name='Comentario',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('titulo', models.CharField(max_length=264)),
                ('cuerpo', models.TextField()),
                ('fecha', models.DateTimeField(verbose_name='publicado')),
                ('contenido', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='cms_put.contenido')),
            ],
        ),
    ]
